# HTML Highlighter
Highlight text strings to styled React elements.

## **Motivation**

*I am a webdeveloper and am teaching friends how to code, and want to display code snippets on my website in a fashion as my code editor does.*

*Now Usually when copying a chunk of code onto a website, it comes weirdly formatted, and only in black and white*

*To fix this, i wrote the ***HTML Highlighter***, which basicially 'highlights' parts of input strings to styled spans.*

*Tiny warning:*<br/>
*Please be aware that my code does not format input strings into custom tab-stops. But it therefor keeps any white-space and tab information available in your input string"*
(You may will have options for this in later major versions)


## **Installation**
*As of now, this module can only be installed for **React** Users.*
Anyways, installation is as simple as using the following npm command:

    npm install --save html-highlight

*Please note that this project builds on Reacts Core Features and will return a React Element as output. Therefor currently **React 18.2.0 needs to be installed**, as its this projects peer dependency.*

### **How do I Use this to style my Code?**
*Okaay so lets go through dis step by step!*

**1st Step - Import Module** 

In your react application, import this modules API to return a styled codeblock.

        import {createStyledCodeblock} from "html-highlight"

**2nd Step - Store Code Snippet**

In order to style a code snippet as code block, you first need to have a code snippet. (This is the Code snippet i used for testing)-

**Important Note:** If you want to preserve spacing and newlines in your snippet, quote strings with the ***`*** character. 

This quotation character differs from " and ', and preserves spacing entirely.

    const code_as_string = `// Register events
    for (const file of eventFiles) {
        const filePath = path.join(eventsPath, file);
        const event = require(filePath);
        console.log("Registering event ->" + event.name);
        if (event.once) client.once(event.name, (...args) => event.execute(...args));
        else client.on(event.name, (...args) => event.execute(...args));
    }`


**3rd Step - Transform Code Snippet** 

Call the ***createStyledCodeblock*** function, and **pass the variable containing the code snippet** you want to style.

    createStyledCodeblock(code_as_string)

___

## **Complete Code**

The following Code shows how to properly import and where to call the html-highlighter as an minimalized all-in-one snippet

    import {createStyledCodeblock} from "html-highlight"

    const code_as_string = `// Register events
    for (const file of eventFiles) {
        const filePath = path.join(eventsPath, file);
        const event = require(filePath);
        console.log("Registering event ->" + event.name);
        if (event.once) client.once(event.name, (...args) => event.execute(...args));
        else client.on(event.name, (...args) => event.execute(...args));
    }`

    const App = () => {

        render(
            <div className="app">
                {createStyledCodeblock(code_as_string)}
            </div>
        )
    }

    export default App
___

## **How does it Work?**

Internally, the module searches for keyword groups like "conditionals", "datatypes", "iterators" and "variables". Each group contains a list of keywords (like "if" and "else" for "conditionals") which will share a styling set.

When one of those keywords is found in your input code, the module will wrap a span around it, fitting to the keywords group.

Finally, css classes are applied for each keyword group, and the styled react-dom version of your input text gets returned as your final output.

___

## **Gotchas**

**Q:** *Can i define own set of keywords and to which css class they map to myself (as you did internally)?*

**A:** *Sadly, in this version this is not possible yet, but will be coming with the next major version bump*

___

## **Bugs and Issues**

If you encounter bugs or issues, you can either follow dis link https://gitlab.com/SilentDraqon/discordbot/-/issues or mail me at silentdraqon@gmail.com

### **Source Code**

You can visit this projects source code at https://gitlab.com/SilentDraqon/html-highlight. 

*Feel free to mess with it and thank youuu for installin my module ^-^.*