import React from "react"
import "./index.scss";

const forEachKeyword = (callback) => {
    const keywords = ["const", "let", "if", "else"]
    for (let keywordIndex = 0; keywordIndex < keywords.length; keywordIndex++) {
        const keyword = keywords[keywordIndex]
        callback(keyword, keywordIndex)
    }
}

const searchKeyword = (line, keyword, keywordIndex, startPos = 0) => {
    const keywordPos = line.indexOf(keyword, startPos)
    if (keywordPos > -1) {
        return {result: true, pos: keywordPos};
    }
    return {result: false, pos: -1};
}

const highlightKeyword = (line, keyword, keywordPos) => {
    const sub0 = line.substring(0, keywordPos)
    const sub1 = line.substring(keywordPos, keywordPos+keyword.length)
    const sub2 = line.substring(keywordPos+keyword.length, line.length)
    let wrapped = <span className="code--keyword">{sub1}</span>
    let result = <span>{sub0}{wrapped}{sub2}</span>
    return result
}

const isCommentLine = (line) => {
    const startsWithDoubleSlash = (line) => line[0] === "/" && line[1] === "/"
    return (startsWithDoubleSlash(line)) ? true : false;
}

const styleLineAsComment = (line) => {
    return <span className="code__line--comment">{line}</span>
}

const forEachLine = (code_as_string, callback) => {
    const code_as_array = code_as_string.split("\n")
    const output_array = []
    for (let lineIndex = 0; lineIndex < code_as_array.length; lineIndex++) {
        let line = code_as_array[lineIndex]
        line = callback(line, lineIndex)
        for(let i=0; i<line.length; i++) {
            output_array.push(line[i])
        }
    }
    return output_array
}

const returnCode = (code_as_string) => {
    return <pre className='code__data'>
        {forEachLine(code_as_string, (line, lineIndex) => {
            if (isCommentLine(line))
                return [styleLineAsComment(line)];
            else {
                let result = []
                const orig_line = line;
                let no_hit = true;
                forEachKeyword((keyword, keywordIndex) => {
                    let keywordsearch = searchKeyword(orig_line, keyword, keywordIndex)
                    if(keywordsearch.result === true)
                        while(keywordsearch.result === true) {
                            no_hit = false;
                            result.push(highlightKeyword(orig_line, keyword, keywordsearch.pos))
                            keywordsearch = searchKeyword(orig_line, keyword, keywordIndex, keyword.length+keywordsearch.pos)
                    }
                })
                if (no_hit) result.push(line)
                return result
            }
        }).map((line, lineIndex) => {
            return <span key={lineIndex} >{line}<br /></span>
        })}
    </pre>
}

export {
    returnCode
}